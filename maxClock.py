# -*- coding: utf-8 -*-
"""
Created on Sun May  6 20:48:33 2018

@author: Paul Schroeder
"""


import threading
import time


import max7219


class MaxClock(max7219.Max):
    """
    Wrapper class for using the MAX7221/7219 as a clock

    :param sec_point_digit: digit 1, ..., 8 of the MAX7219 used for the \
second points. Default is 5.
    :type sec_point_digit: int
    :param common_anode: decides if the MAX7219 is driven as command anode. \
Default is False.
    :type common_anode: bool

    Following variables are instance variables and settable via constructor:

    :ivar sec_point_digit: initial value: 5
    :ivar verbose: decides weather there will be more informative console \
output. Default is False.
    :type verbose: bool

    :raises TypeError: not an int
    :raises ValueError: not a bool
    """
    def __init__(self, sec_point_digit=5, common_anode=False):
        # sec_point_digit
        if not isinstance(sec_point_digit, int):
            raise TypeError("not an int")
        if sec_point_digit not in range(1, self._max_digits+1):
            raise IndexError("out of range")

        # polarity
        if common_anode not in [True, False, 0, 1]:
            raise ValueError("not a bool")

        super().__init__(
            lat_pin=3,
            number_of_digits=sec_point_digit,
            common_anode=common_anode

        )
        self.sec_point_digit = sec_point_digit
        self.verbose = False
        self._timer = None
        self._sec_point = False

    def set_sec_points(self, on_off=True):
        """
        Sets or clears the second points. Called without any argument makes \
them light up, by setting all the segments of
        the digit (see sec_point_digit).

        :param on_off: weather to turn second points on or off
        :type bool:
        :return: the Clock instance
        :rtype: maxClock.MaxClock

        :raises ValueError: no bool given
        """
        # polarity
        if on_off not in [True, False, 0, 1]:
            raise ValueError("not a bool")

        self._sec_point = on_off

        self.set_digit(self.sec_point_digit, 0xff if on_off else 0x00)
        return self

    def clear_sec_points(self):
        """
        Clears the second points. This method is equivalent to \
`set_sec_points(False)`.

        :return: the Clock instance
        :rtype: maxClock.MaxClock
        """
        self.set_sec_points(False)
        return self

    def update_time(self):
        """
        Update the time displayed on the clock and the console with the \
actual time. Time on the console will have the
        following format:
            %H:%M:%S
        Time on the MaxClock will have following format:
            %H:%M

        :return: the Clock instance
        :rtype: maxClock.MaxClock
        """

        if self.verbose:
            print(time.strftime("\r> %H:%M:%S"), end='')

        self.write_string(time.strftime(" %H%M"))
        return self

    def toggle_sec_point(self):
        """
        Toggles the second points (if off to on, if on to off).

        :return: the Clock instance
        :rtype: maxClock.MaxClock
        """
        self.set_sec_points(not self._sec_point)
        return self

    def stop(self):
        """
        Stops the timer thread which handles the second point blinking and \
the time update.

        :return: the Clock instance
        :rtype: maxClock.MaxClock
        """
        self._timer.cancel()
        return self

    def start(self):
        """
        Starts the clock i.e. a thread which handles the time update and \
second points.

        :return: the Clock instance
        :rtype: maxClock.MaxClock
        """

        self.update_time()
        self.toggle_sec_point()

        self._timer = threading.Timer(1.0, self.start)
        self._timer.start()
        return self


if __name__ == "__main__":
    clock = MaxClock(common_anode=True)
    clock.verbose = True
    clock.start()
    while True:
        time.sleep(1)
