#!/usr/bin/env bash

UNAME_OS="$(uname -s)"
case "${UNAME_OS}" in
    Linux*)     MACHINE="linux";;
    Darwin*)    MACHINE="darwin";;
    CYGWIN*)    MACHINE="windows";;
    MSYS*)      MACHINE="windows";;
    MINGW*)     MACHINE="windows";;
    *)          MACHINE="UNKNOWN:${UNAME_IDENT}"
esac

UNAME_ARCH="$(uname -m)"
case "${UNAME_ARCH}" in
    x86)    ARCH="386";;
    i?86)   ARCH="386";;
    amd64)  ARCH="amd64";;
    x86_64) ARCH="amd64";;
    *)      ARCH="UNKNOWN:${UNAME_IDENT}"
esac

echo "Running $MACHINE on $ARCH"
[ "$MACHINE" == "win" ] && EXT=".exe"

[ ! -f ./gitlab-runner ] && wget -O gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-${MACHINE}-${ARCH}${EXT}
chmod +x gitlab-runner

./gitlab-runner exec docker conduct_tests