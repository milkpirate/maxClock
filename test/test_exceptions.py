# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
#
# """
# @author: Paul Schroeder
# """
#
#
# import maxClock
# import pytest
#
#
# @pytest.mark.parametrize("sec_point_digit, excpt", [
#     pytest.param(-1,        IndexError),
#     pytest.param(9,         IndexError),
#     pytest.param("no int",  TypeError),
# ])
# def test_constructor_sec_point_digit_exceptions(sec_point_digit, excpt):
#     with pytest.raises(excpt):
#         maxClock.MaxClock(sec_point_digit=sec_point_digit)
#
#
# @pytest.mark.parametrize("common_anode, excpt", [
#     pytest.param(-1,                ValueError),
#     pytest.param(2,                 ValueError),
#     pytest.param("no bool like",    ValueError),
# ])
# def test_constructor_common_anode_exceptions(common_anode, excpt):
#     with pytest.raises(excpt):
#         maxClock.MaxClock(common_anode=common_anode)
