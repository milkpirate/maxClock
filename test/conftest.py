#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""


import os
import pytest
import sys


from test import mock_littleWire
sys.modules['littleWire'] = mock_littleWire


@pytest.fixture()
def is_travis_ci(request):
    is_travis = os.environ.get("TRAVIS", False)  # returns "true"/"false"
    return is_travis == "true"
